#pragma once

namespace CLI_Clicker {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for sklep
	/// </summary>
	public ref class sklep : public System::Windows::Forms::Form
	{
	public:
		sklep(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~sklep()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  buyHP;
	private: System::Windows::Forms::Button^  buyATTACK;
	private: System::Windows::Forms::Button^  closeShop;
	private: System::Windows::Forms::PictureBox^  pictureHP;
	private: System::Windows::Forms::PictureBox^  pictureAT;
	protected:



	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(sklep::typeid));
			this->buyHP = (gcnew System::Windows::Forms::Button());
			this->buyATTACK = (gcnew System::Windows::Forms::Button());
			this->closeShop = (gcnew System::Windows::Forms::Button());
			this->pictureHP = (gcnew System::Windows::Forms::PictureBox());
			this->pictureAT = (gcnew System::Windows::Forms::PictureBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureHP))->BeginInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureAT))->BeginInit();
			this->SuspendLayout();
			// 
			// buyHP
			// 
			this->buyHP->Location = System::Drawing::Point(161, 42);
			this->buyHP->Name = L"buyHP";
			this->buyHP->Size = System::Drawing::Size(97, 23);
			this->buyHP->TabIndex = 0;
			this->buyHP->Text = L"KUP - 10 PKT";
			this->buyHP->UseVisualStyleBackColor = true;
			this->buyHP->Click += gcnew System::EventHandler(this, &sklep::buyHP_Click);
			// 
			// buyATTACK
			// 
			this->buyATTACK->Location = System::Drawing::Point(161, 107);
			this->buyATTACK->Name = L"buyATTACK";
			this->buyATTACK->Size = System::Drawing::Size(97, 23);
			this->buyATTACK->TabIndex = 1;
			this->buyATTACK->Text = L"KUP - 20 PKT";
			this->buyATTACK->UseVisualStyleBackColor = true;
			this->buyATTACK->Click += gcnew System::EventHandler(this, &sklep::buyATTACK_Click);
			// 
			// closeShop
			// 
			this->closeShop->Location = System::Drawing::Point(117, 188);
			this->closeShop->Name = L"closeShop";
			this->closeShop->Size = System::Drawing::Size(119, 23);
			this->closeShop->TabIndex = 2;
			this->closeShop->Text = L"ZAMKNIJ SKLEP";
			this->closeShop->UseVisualStyleBackColor = true;
			this->closeShop->Click += gcnew System::EventHandler(this, &sklep::closeShop_Click);
			// 
			// pictureHP
			// 
			this->pictureHP->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureHP.Image")));
			this->pictureHP->Location = System::Drawing::Point(55, 26);
			this->pictureHP->Name = L"pictureHP";
			this->pictureHP->Size = System::Drawing::Size(100, 50);
			this->pictureHP->TabIndex = 3;
			this->pictureHP->TabStop = false;
			// 
			// pictureAT
			// 
			this->pictureAT->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureAT.Image")));
			this->pictureAT->Location = System::Drawing::Point(55, 94);
			this->pictureAT->Name = L"pictureAT";
			this->pictureAT->Size = System::Drawing::Size(100, 50);
			this->pictureAT->TabIndex = 4;
			this->pictureAT->TabStop = false;
			// 
			// sklep
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->pictureAT);
			this->Controls->Add(this->pictureHP);
			this->Controls->Add(this->closeShop);
			this->Controls->Add(this->buyATTACK);
			this->Controls->Add(this->buyHP);
			this->Name = L"sklep";
			this->Text = L"sklep";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureHP))->EndInit();
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureAT))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion

	private: System::Void buyHP_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void buyATTACK_Click(System::Object^  sender, System::EventArgs^  e) {
	}
	private: System::Void closeShop_Click(System::Object^  sender, System::EventArgs^  e) {
		this->Close();
	}
	};
}