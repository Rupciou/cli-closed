#pragma once
#include "MyForm1.h"

namespace CLI_Clicker {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;

	/// <summary>
	/// Summary for MyForm
	/// </summary>
	public ref class MyForm : public System::Windows::Forms::Form
	{
	public:
		MyForm(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  startButton;
	private: System::Windows::Forms::Button^  wyjscieButton;
	private: System::Windows::Forms::PictureBox^  pictureStart;
	protected:


	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MyForm::typeid));
			this->startButton = (gcnew System::Windows::Forms::Button());
			this->wyjscieButton = (gcnew System::Windows::Forms::Button());
			this->pictureStart = (gcnew System::Windows::Forms::PictureBox());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureStart))->BeginInit();
			this->SuspendLayout();
			// 
			// startButton
			// 
			this->startButton->Location = System::Drawing::Point(112, 155);
			this->startButton->Name = L"startButton";
			this->startButton->Size = System::Drawing::Size(75, 23);
			this->startButton->TabIndex = 0;
			this->startButton->Text = L"START";
			this->startButton->UseVisualStyleBackColor = true;
			this->startButton->Click += gcnew System::EventHandler(this, &MyForm::startButton_Click);
			// 
			// wyjscieButton
			// 
			this->wyjscieButton->Location = System::Drawing::Point(112, 208);
			this->wyjscieButton->Name = L"wyjscieButton";
			this->wyjscieButton->Size = System::Drawing::Size(75, 23);
			this->wyjscieButton->TabIndex = 1;
			this->wyjscieButton->Text = L"WYJSCIE";
			this->wyjscieButton->UseVisualStyleBackColor = true;
			this->wyjscieButton->Click += gcnew System::EventHandler(this, &MyForm::wyjscieButton_Click);
			// 
			// pictureStart
			// 
			this->pictureStart->Anchor = static_cast<System::Windows::Forms::AnchorStyles>((System::Windows::Forms::AnchorStyles::Top | System::Windows::Forms::AnchorStyles::Bottom));
			this->pictureStart->BackColor = System::Drawing::SystemColors::Control;
			this->pictureStart->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureStart.Image")));
			this->pictureStart->Location = System::Drawing::Point(74, 42);
			this->pictureStart->Name = L"pictureStart";
			this->pictureStart->Size = System::Drawing::Size(152, 22);
			this->pictureStart->SizeMode = System::Windows::Forms::PictureBoxSizeMode::AutoSize;
			this->pictureStart->TabIndex = 2;
			this->pictureStart->TabStop = false;
			// 
			// MyForm
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->pictureStart);
			this->Controls->Add(this->wyjscieButton);
			this->Controls->Add(this->startButton);
			this->Name = L"MyForm";
			this->Text = L"MyForm";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureStart))->EndInit();
			this->ResumeLayout(false);
			this->PerformLayout();

		}
#pragma endregion
	private: System::Void wyjscieButton_Click(System::Object^  sender, System::EventArgs^  e) {
		Close();
	}
	private: System::Void startButton_Click(System::Object^  sender, System::EventArgs^  e) {
		MyForm1^ okno = gcnew MyForm1;
		okno->Show();
		
			
		
	}
};
}
