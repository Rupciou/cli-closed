#pragma once
#include"sklep.h"
#include"postac.h"

namespace CLI_Clicker {

	using namespace System;
	using namespace System::ComponentModel;
	using namespace System::Collections;
	using namespace System::Windows::Forms;
	using namespace System::Data;
	using namespace System::Drawing;
	
	/// <summary>
	/// Summary for MyForm1
	/// </summary>
	public ref class MyForm1 : public System::Windows::Forms::Form
	{
	public:

		int sumaAtak = 0;
		int hpStworek = 80;
		int hpTy = 100;
		MyForm1(void)
		{
			InitializeComponent();
			//
			//TODO: Add the constructor code here
			//
		}

	protected:
		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		~MyForm1()
		{
			if (components)
			{
				delete components;
			}
		}
	private: System::Windows::Forms::Button^  buttonAtak;
	protected:


	private: System::Windows::Forms::Button^  buttonUciekaj;
	private: System::Windows::Forms::Button^  buttonPostac;



	private: System::Windows::Forms::Button^  buttonSklep;

	private: System::Windows::Forms::PictureBox^  pictureWrog;
	private: System::Windows::Forms::Button^  buttonExit;

	protected:

	private:
		/// <summary>
		/// Required designer variable.
		/// </summary>
		System::ComponentModel::Container ^components;

#pragma region Windows Form Designer generated code
		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		void InitializeComponent(void)
		{
			System::ComponentModel::ComponentResourceManager^  resources = (gcnew System::ComponentModel::ComponentResourceManager(MyForm1::typeid));
			this->buttonAtak = (gcnew System::Windows::Forms::Button());
			this->buttonUciekaj = (gcnew System::Windows::Forms::Button());
			this->buttonPostac = (gcnew System::Windows::Forms::Button());
			this->buttonSklep = (gcnew System::Windows::Forms::Button());
			this->pictureWrog = (gcnew System::Windows::Forms::PictureBox());
			this->buttonExit = (gcnew System::Windows::Forms::Button());
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureWrog))->BeginInit();
			this->SuspendLayout();
			// 
			// buttonAtak
			// 
			this->buttonAtak->Location = System::Drawing::Point(76, 153);
			this->buttonAtak->Name = L"buttonAtak";
			this->buttonAtak->Size = System::Drawing::Size(75, 23);
			this->buttonAtak->TabIndex = 0;
			this->buttonAtak->Text = L"ATAK!";
			this->buttonAtak->UseVisualStyleBackColor = true;
			this->buttonAtak->Click += gcnew System::EventHandler(this, &MyForm1::buttonAtak_Click);
			// 
			// buttonUciekaj
			// 
			this->buttonUciekaj->Location = System::Drawing::Point(76, 192);
			this->buttonUciekaj->Name = L"buttonUciekaj";
			this->buttonUciekaj->Size = System::Drawing::Size(75, 23);
			this->buttonUciekaj->TabIndex = 2;
			this->buttonUciekaj->Text = L"UCIEKAJ!";
			this->buttonUciekaj->UseVisualStyleBackColor = true;
			this->buttonUciekaj->Click += gcnew System::EventHandler(this, &MyForm1::buttonUciekaj_Click);
			// 
			// buttonPostac
			// 
			this->buttonPostac->Location = System::Drawing::Point(197, 192);
			this->buttonPostac->Name = L"buttonPostac";
			this->buttonPostac->Size = System::Drawing::Size(75, 23);
			this->buttonPostac->TabIndex = 3;
			this->buttonPostac->Text = L"POSTAC";
			this->buttonPostac->UseVisualStyleBackColor = true;
			this->buttonPostac->Click += gcnew System::EventHandler(this, &MyForm1::buttonPostac_Click);
			// 
			// buttonSklep
			// 
			this->buttonSklep->Location = System::Drawing::Point(197, 153);
			this->buttonSklep->Name = L"buttonSklep";
			this->buttonSklep->Size = System::Drawing::Size(75, 23);
			this->buttonSklep->TabIndex = 4;
			this->buttonSklep->Text = L"SKLEP";
			this->buttonSklep->UseVisualStyleBackColor = true;
			this->buttonSklep->Click += gcnew System::EventHandler(this, &MyForm1::buttonSklep_Click);
			// 
			// pictureWrog
			// 
			this->pictureWrog->Image = (cli::safe_cast<System::Drawing::Image^>(resources->GetObject(L"pictureWrog.Image")));
			this->pictureWrog->Location = System::Drawing::Point(-9, 12);
			this->pictureWrog->Name = L"pictureWrog";
			this->pictureWrog->Size = System::Drawing::Size(300, 100);
			this->pictureWrog->TabIndex = 5;
			this->pictureWrog->TabStop = false;
			// 
			// buttonExit
			// 
			this->buttonExit->Location = System::Drawing::Point(197, 226);
			this->buttonExit->Name = L"buttonExit";
			this->buttonExit->Size = System::Drawing::Size(75, 23);
			this->buttonExit->TabIndex = 6;
			this->buttonExit->Text = L"WYJSCIE";
			this->buttonExit->UseVisualStyleBackColor = true;
			this->buttonExit->Click += gcnew System::EventHandler(this, &MyForm1::buttonExit_Click);
			// 
			// MyForm1
			// 
			this->AutoScaleDimensions = System::Drawing::SizeF(6, 13);
			this->AutoScaleMode = System::Windows::Forms::AutoScaleMode::Font;
			this->ClientSize = System::Drawing::Size(284, 261);
			this->Controls->Add(this->buttonExit);
			this->Controls->Add(this->pictureWrog);
			this->Controls->Add(this->buttonSklep);
			this->Controls->Add(this->buttonPostac);
			this->Controls->Add(this->buttonUciekaj);
			this->Controls->Add(this->buttonAtak);
			this->Name = L"MyForm1";
			this->Text = L"MyForm1";
			(cli::safe_cast<System::ComponentModel::ISupportInitialize^>(this->pictureWrog))->EndInit();
			this->ResumeLayout(false);

		}
#pragma endregion
	
private: System::Void buttonExit_Click(System::Object^  sender, System::EventArgs^  e) {

	this->Close();

}
private: System::Void buttonAtak_Click(System::Object^  sender, System::EventArgs^  e) {
	sumaAtak++;
	
}
private: System::Void buttonSklep_Click(System::Object^  sender, System::EventArgs^  e) {
	sklep^ oknoSklepu = gcnew sklep;
	oknoSklepu->Show();

}
private: System::Void buttonPostac_Click(System::Object^  sender, System::EventArgs^  e) {
	postac^ oknoPostaci = gcnew postac;
	oknoPostaci->Show();

}
private: System::Void buttonUciekaj_Click(System::Object^  sender, System::EventArgs^  e) {
	hpTy = hpTy - 10;
}
};
}
